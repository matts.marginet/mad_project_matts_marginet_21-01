package com.example.projectMattsMarginet.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.projectMattsMarginet.models.BasketItem

// See https://developer.android.com/training/data-storage/room
@Database(entities = [BasketItem::class], version = 1)
abstract class BasketDatabase: RoomDatabase() {
    abstract fun basketItemDao() : BasketItemDao
}