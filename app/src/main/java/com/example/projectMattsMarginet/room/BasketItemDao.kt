package com.example.projectMattsMarginet.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.projectMattsMarginet.models.BasketItem

// See https://developer.android.com/training/data-storage/room
@Dao
interface BasketItemDao {
    @Insert
    fun insert(basketItem: BasketItem)

    @Delete
    fun delete(basketItem: BasketItem)

    @Query("DELETE FROM basketItem")
    fun deleteAll()

    @Query("SELECT * FROM basketitem")
    fun getAll() : List<BasketItem>
}