package com.example.projectMattsMarginet.repositories

import android.util.Log
import com.example.projectMattsMarginet.models.UserProfile
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.google.firebase.firestore.AggregateSource
import com.google.firebase.firestore.firestore
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.tasks.await

interface AuthRepository {
    suspend fun register(
        userProfile: UserProfile,
        password: String
    ): String
    suspend fun login(
        email: String,
        password: String
    ): String
    suspend fun getCurrentUserProfile(): UserProfile?
    fun logout()
    suspend fun getNumberOfUsers(): Long
}

@Suppress("LiftReturnOrAssignment")
class AuthRepositoryImpl : AuthRepository {
    override suspend fun register(
        userProfile: UserProfile,
        password: String
    ): String {
        try {
            // Register new user in Firebase authentication
            val authResult = Firebase.auth
                .createUserWithEmailAndPassword(userProfile.email, password)
                .await()

            // Check if registration was successful
            if (authResult == null || authResult.user == null) {
                currentUserProfile = null
                return "User could not be registered"
            }

            // Get the UID of the new user and update the user profile
            val uid = authResult.user!!.uid
            userProfile.uid = uid

            // Get the Firebase messaging token and update the user profile
            val token = FirebaseMessaging.getInstance().token.await()
            Log.d(LOG_TAG, "Token (register): $token")
            if (userProfile.token != token) {
                userProfile.token = token
            }

            // Store the user profile in the Firestore database
            Firebase.firestore
                .collection(USERS_COLLECTION)
                .document(uid)
                .set(userProfile)
                .await()

            //Set the currentUserProfile
            currentUserProfile = userProfile
            return ""
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            currentUserProfile = null
            return e.message ?: ""
        }
    }

    override suspend fun login(
        email: String,
        password: String
    ): String {
        try {
            // Login with Firebase authentication
            val authResult = Firebase.auth
                .signInWithEmailAndPassword(email, password)
                .await()

            // Check the authentication result
            if (authResult == null || authResult.user == null) {
                currentUserProfile = null
                return "User could not be logged in"
            }

            // Get the current user profile
            val userProfile = getCurrentUserProfile()

            // If user profile found return it
            return if (userProfile != null) "" else "User could not be logged in"
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            currentUserProfile = null
            return e.message ?: ""
        }
    }

    override suspend fun getCurrentUserProfile(): UserProfile? {
        try {
            // Get UID from currently logged in user
            val uid = Firebase.auth.currentUser?.uid

            // Check if user was logged in
            if (uid.isNullOrBlank()) {
                currentUserProfile = null
                return null
            }

            // If currentUser profile is the same as currently logged in use return it
            if (currentUserProfile != null && currentUserProfile!!.uid == uid)
                return currentUserProfile

            // Get the user profile from the Firestore database
            val userProfileSnapshot = Firebase.firestore
                .collection(USERS_COLLECTION)
                .document(uid)
                .get()
                .await()

            // Check if a user profile was found
            if (userProfileSnapshot.exists()) {
                // Get the user profile from the result
                val userProfile = userProfileSnapshot
                    .toObject(UserProfile::class.java)

                // Get the Firebase messaging token
                val token = FirebaseMessaging.getInstance().token.await()

                // Check if user profile token is different from found token
                if (userProfile!!.token != token) {
                    // Set the user profile token
                    userProfile.token = token

                    // Store the user profile in the Firestore database
                    Firebase.firestore
                        .collection(USERS_COLLECTION)
                        .document(uid)
                        .set(userProfile)
                        .await()
                }

                // Set the currentUserProfile
                currentUserProfile = userProfile
                return userProfile
            }

            currentUserProfile = null
            return null
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            currentUserProfile = null
            return null
        }
    }

    override fun logout() {
        // Logout user in Firebase authentication
        Firebase.auth
            .signOut()

        // Clear the currentUserProfile
        currentUserProfile = null
    }

    override suspend fun getNumberOfUsers(): Long {
        try {
            // Count the number of user profiles in Firestore database
            val snapshot = Firebase.firestore
                .collection(USERS_COLLECTION)
                .count()
                .get(AggregateSource.SERVER)
                .await()

            return snapshot.count
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return 0
        }
    }

    // Define a companion object
    companion object {
        // Define properties
        var currentUserProfile:UserProfile? = null
            private set

        // Define Log tag
        private const val LOG_TAG = "AuthRepository"

        // Define Users collection name
        private const val USERS_COLLECTION = "users"
    }
}