package com.example.projectMattsMarginet.repositories

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.models.UserProfile
import com.google.firebase.Firebase
import com.google.firebase.firestore.AggregateSource
import com.google.firebase.firestore.Filter
import com.google.firebase.firestore.firestore
import com.google.firebase.firestore.toObjects
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.storage
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.util.UUID

interface ProductRepository {
    suspend fun savePicture(
        bitmap: Bitmap
    ): String
    suspend fun saveProduct(
        product: Product
    ): String
    suspend fun getProductList(
        userProfile: UserProfile?
    ): List<Product>
    suspend fun getProductByIdList(
        productIdList: List<String>
    ): List<Product>
    suspend fun getProduct(
        productId: String
    ): Product?
    suspend fun setProductsInStock(
        productIdList: List<String>
    ): String
    suspend fun getNumberOfProducts(): Long
}

@Suppress("LiftReturnOrAssignment", "SimplifyBooleanWithConstants")
class ProductRepositoryImpl(
    private val context: Context
): ProductRepository {
    override suspend fun savePicture(bitmap: Bitmap): String {
        try {
            // Convert bitmap to byte array
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val pictureData = byteArrayOutputStream.toByteArray()

            // Free bitmap from memory
            bitmap.recycle()

            // Store byte array to file in cache directory
            val fileName = "${UUID.randomUUID()}.jpg"
            val productPictureFile = File(context.cacheDir, fileName)
            productPictureFile.writeBytes(pictureData)

            // Return the filename the bitmap was stored in
            return fileName
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun saveProduct(
        product: Product
    ): String {
        try {
            // If new product set product id
            if (product.productId.isBlank()) {
                product.productId = Firebase.firestore
                    .collection(PRODUCTS_COLLECTION)
                    .document()
                    .id
            }

            // Store the product in the Firestore database
            Firebase.firestore
                .collection(PRODUCTS_COLLECTION)
                .document(product.productId)
                .set(product)
                .await()

            // Load the picture from the file in cache directory into byte array
            val fis = FileInputStream(File(context.cacheDir, product.pictureFileName))
            val fileSize = fis.available()
            val pictureData = ByteArray(fileSize)
            fis.read(pictureData)
            fis.close()

            // Store picture byte array if Storage database
            Firebase.storage.reference
                .child(PRODUCT_IMAGE_CHILD)
                .child(product.pictureFileName)
                .putBytes(pictureData)
                .await()

            return ""
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun getProductList(
        userProfile: UserProfile?
    ) :List<Product> {
        try {
            // Check if user profile is Admin
            if (userProfile != null && userProfile.isAdmin) {
                // Get all the products from the Firestore database
                // and call loadPictures
                return Firebase.firestore
                    .collection(PRODUCTS_COLLECTION)
                    .get()
                    .await()
                    .toObjects<Product>()
                    .loadPictures()
            }

            // Get the active products from the Firestore database
            // and call loadPictures
            return Firebase.firestore
                .collection(PRODUCTS_COLLECTION)
                .where(
                    Filter.equalTo("isActive", true)
                )
                .get()
                .await()
                .toObjects<Product>()
                .loadPictures()
        } catch (e: Exception){
            Log.e(LOG_TAG, e.toString())
            return emptyList()
        }
    }

    override suspend fun getProductByIdList(productIdList: List<String>): List<Product> {
        try {
            // Get the products based on the product id's provided from the Firestore database
            // and call loadPictures
            return Firebase.firestore
                .collection(PRODUCTS_COLLECTION)
                .where(
                    Filter.inArray("productId", productIdList)
                )
                .get()
                .await()
                .toObjects<Product>()
                .loadPictures()
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return emptyList()
        }
    }

    override suspend fun getProduct(
        productId: String
    ): Product? {
        try {
            // Get the product from the Firestore database
            val productSnapShot = Firebase.firestore
                .collection(PRODUCTS_COLLECTION)
                .document(productId)
                .get()
                .await()

            // Check if product found
            if (productSnapShot.exists()) {
                // Get product from result
                val product = productSnapShot
                    .toObject(Product::class.java)!!

                // Load picture
                product.loadPicture()

                return product
            }

            return null
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return null
        }
    }

    override suspend fun setProductsInStock(productIdList: List<String>): String {
        try {
            val errorList = mutableListOf<String>()

            // Get the products based on the product id's provided from the Firestore database
            val productList = getProductByIdList(productIdList)

            // For each product
            productList.forEach { product ->
                // Set in stock
                product.inStock = true

                // Store product in Firestore database
                errorList.add(saveProduct(product))
            }
            return errorList.joinToString(", ")
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun getNumberOfProducts(): Long {
        try {
            // Count the number of products in Firestore database
            val snapshot = Firebase.firestore
                .collection(PRODUCTS_COLLECTION)
                .count()
                .get(AggregateSource.SERVER)
                .await()

            return snapshot.count
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return 0
        }
    }

    private suspend fun List<Product>.loadPictures(): List<Product> {
        // For each product load picture
        return this.map {product ->
            product.loadPicture()
            product
        }
    }

    private suspend fun Product.loadPicture() {
        // Check if picture file name is set
        if (this.pictureFileName.isNotBlank()) {
            val pictureFile = File(context.cacheDir, this.pictureFileName)

            // Check if picture file exists in cache directory
            if (pictureFile.exists() == false) {
                // Load picture from Storage database and store file in cache directory
                productImageStorageReference
                    .child(this.pictureFileName)
                    .getFile(pictureFile)
                    .await()
            }
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        private const val LOG_TAG = "ProductRepository"

        // Define collection names
        private const val PRODUCTS_COLLECTION = "products"

        // Define child names
        private const val PRODUCT_IMAGE_CHILD = "productImage"

        // Define Storage References
        private val productImageStorageReference: StorageReference = Firebase.storage.reference
            .child(PRODUCT_IMAGE_CHILD)
    }
}