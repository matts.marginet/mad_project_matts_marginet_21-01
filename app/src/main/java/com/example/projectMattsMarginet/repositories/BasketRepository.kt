package com.example.projectMattsMarginet.repositories

import android.util.Log
import com.example.projectMattsMarginet.models.BasketItem
import com.example.projectMattsMarginet.room.BasketItemDao

interface BasketRepository {
    suspend fun addProductToBasket(
        productId: String
    ): String
    suspend fun deleteProductFromBasket(
        productId: String
    ): String
    suspend fun clearBasket(
    ) : String
    suspend fun getProductInBasketList(
    ): List<String>
}

@Suppress("LiftReturnOrAssignment")
class BasketRepositoryImpl(
    private val basketItemDao: BasketItemDao
): BasketRepository {
    override suspend fun addProductToBasket(productId: String): String {
        try {
            // Create a basket item
            val basketItem = BasketItem(productId)

            // Insert basket item in Room database
            basketItemDao.insert(basketItem)

            return ""
        } catch (e: Exception){
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun deleteProductFromBasket(productId: String): String {
        try {
            // Create basket item
            val basketItem = BasketItem(productId)

            // Delete basket item from Room database
            basketItemDao.delete(basketItem)

            return ""
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun clearBasket(): String {
        try {
            // Delete all basket items from Room database
            basketItemDao.deleteAll()

            return ""
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }

    override suspend fun getProductInBasketList(): List<String> {
        try {
            // Get all basket items from Room database
            // and map them to a list of product id's
            return basketItemDao.getAll()
                .map { basketItem ->
                    basketItem.productId
                }
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return emptyList()
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        private const val LOG_TAG = "BasketRepository"
    }
}