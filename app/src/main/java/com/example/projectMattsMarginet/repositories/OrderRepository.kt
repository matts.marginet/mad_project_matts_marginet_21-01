package com.example.projectMattsMarginet.repositories

import android.util.Log
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.OrderItem
import com.example.projectMattsMarginet.models.OrderStatus
import com.example.projectMattsMarginet.models.OrderStatusFilter
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.models.UserProfile
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.google.firebase.firestore.AggregateSource
import com.google.firebase.firestore.Filter
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.firestore
import com.google.firebase.firestore.toObjects
import kotlinx.coroutines.tasks.await

interface OrderRepository {
    suspend fun saveOrder(
        order: Order
    ): String
    suspend fun getOrderListForAdmin(
        orderStatusFilterList: List<OrderStatusFilter>
    ): List<Order>
    suspend fun getOrderListForProfile(): List<Order>
    suspend fun getOrder(
        orderId: String
    ): Order?
    suspend fun getNumberOfOrders(): Long
}

@Suppress("SimplifyBooleanWithConstants", "LiftReturnOrAssignment")
class OrderRepositoryImpl: OrderRepository {
    @Suppress("SpellCheckingInspection", "ControlFlowWithEmptyBody")
    override suspend fun saveOrder(order: Order): String {
        try {
            var previous: Order? = null

            // If new order set UID
            if (order.uid.isBlank()) {
                order.uid = Firebase.auth.currentUser!!.uid
            }

            // If new order set order id
            // Else get previous version of order from Firestore database
            if (order.orderId.isBlank()) {
                // Get new order id from Firestore database
                order.orderId = Firebase.firestore
                    .collection(ORDERS_COLLECTION)
                    .document()
                    .id
            } else {
                previous = getOrder(order.orderId)
            }

            // If new order set order number
            if (order.orderNumber <= 0) {
                // Get last order from Firestore database
                val orderList = Firebase.firestore
                    .collection(ORDERS_COLLECTION)
                    .orderBy("createdAt", Query.Direction.DESCENDING)
                    .limit(1)
                    .get()
                    .await()
                    .toObjects<Order>()

                // If last order found take order number and add 1
                // Else set oder number to 1, this is the first order
                if (orderList.size == 1) {
                    order.orderNumber = orderList[0].orderNumber + 1
                } else {
                    order.orderNumber = 1
                }
            }

            // Store the order in the Firestore database
            Firebase.firestore
                .collection(ORDERS_COLLECTION)
                .document(order.orderId)
                .set(order)
                .await()

            // For each order item
            order.orderItems.forEach { orderItem ->
                // If new order item set order item id
                if (orderItem.orderItemId.isBlank()) {
                    // Get new order item id from Firestore database
                    orderItem.orderItemId = Firebase.firestore
                        .collection(ORDERS_COLLECTION)
                        .document(order.orderId)
                        .collection(ORDER_ITEM_COLLECTION)
                        .document()
                        .id
                }

                // Store order item in the Firestore database
                Firebase.firestore
                    .collection(ORDERS_COLLECTION)
                    .document(order.orderId)
                    .collection(ORDER_ITEM_COLLECTION)
                    .document(orderItem.orderItemId)
                    .set(orderItem)
                    .await()
            }

            // Check if previous version of the order had status InProgress
            // and current order status is Ready
            if (previous != null
                && previous.status == OrderStatus.IN_PROGRESS
                && order.status == OrderStatus.READY) {
                val token = AuthRepositoryImpl.currentUserProfile?.token
                if (token.isNullOrBlank() == false) {
                    // Send Notification to user of the order,
                    // saying that the order is ready for pickup

                    // TODO send message
                    // FirebaseMessaging.getInstance().send()

                    // Na veel zoeken heb ik het versturen van een notificatie niet aan de praat gekregen.
                    // Het zou moeten lukken via FirebaseMessaging.getInstance().send(), maar dat is de oude manier.
                    // De nieuwe manier is via de api van google, maar daar liep ik vast op de authenticatie.
                }
            }

            return ""
        }
        catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return e.message ?: ""
        }
    }


    override suspend fun getOrderListForAdmin(orderStatusFilterList: List<OrderStatusFilter>): List<Order> {
        try {
            // Filter the order status list to only keep the checked items
            // Map the list to a list of order status enum ordinals
            val statusOrdinalList = orderStatusFilterList
                .filter { filter ->
                    filter.checked
                }.map { filter ->
                    filter.status.ordinal
                }

            // Get all the orders with status enum ordinal in the filter list
            // from the Firestore database
            return Firebase.firestore
                .collection(ORDERS_COLLECTION)
                .whereIn("statusOrdinal", statusOrdinalList)
                .orderBy("statusOrdinal", Query.Direction.ASCENDING)
                .orderBy("orderNumber")
                .get()
                .await()
                .toObjects<Order>()
                .map { order ->
                    // For each order found
                    // If order has user assigned
                    if (order.uid.isNotBlank()) {
                        // Get the user profile from the Firestore database
                        val userProfileSnapshot = Firebase.firestore
                            .collection(USERS_COLLECTION)
                            .document(order.uid)
                            .get()
                            .await()

                        // Check if user profile found
                        if (userProfileSnapshot.exists()) {
                            // Get the user profile from the result
                            order.userProfile = userProfileSnapshot
                                .toObject(UserProfile::class.java)
                        }
                    }
                    order
                }
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return emptyList()
        }
    }

    override suspend fun getOrderListForProfile(
    ): List<Order> {
        try {
            // Check if user logged in, if not return empty list
            if (Firebase.auth.currentUser == null) return emptyList()

            // Get all orders assigned to the current user from the Firestore database
            return Firebase.firestore
                .collection(ORDERS_COLLECTION)
                .where(
                    Filter.equalTo("uid", Firebase.auth.currentUser!!.uid)
                )
                .orderBy("orderNumber")
                .get()
                .await()
                .toObjects<Order>()
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return emptyList()
        }
    }

    override suspend fun getOrder(orderId: String): Order? {
        try {
            // Get the order with provide order id from the Firestore database
            val orderSnapshot = Firebase.firestore
                .collection(ORDERS_COLLECTION)
                .document(orderId)
                .get()
                .await()

            // Check if order found
            if (orderSnapshot.exists()) {
                // Get the order from the result
                val order = orderSnapshot.toObject(Order::class.java)!!

                // Get the order items from the found order from the Firestore database
                val orderItemList = Firebase.firestore
                    .collection(ORDERS_COLLECTION)
                    .document(orderId)
                    .collection(ORDER_ITEM_COLLECTION)
                    .get()
                    .await()
                    .toObjects<OrderItem>()

                // For each order item found
                orderItemList.forEach { orderItem ->
                    // Get the product of the found order item from the Firestore database
                    val productSnapshot = Firebase.firestore
                        .collection(PRODUCTS_COLLECTION)
                        .document(orderItem.productId)
                        .get()
                        .await()

                    // Check if product was found
                    if (productSnapshot.exists()) {
                        // Get the product from the result
                        orderItem.product = productSnapshot.toObject(Product::class.java)
                    }

                    // Add the found order item to the found order
                    order.orderItems.add(orderItem)
                }

                // Get the user profile assigned to the found order from the Firestore database
                val userProfileSnapshot = Firebase.firestore
                    .collection(USERS_COLLECTION)
                    .document(order.uid)
                    .get()
                    .await()

                // Check if user profile was found
                if (userProfileSnapshot.exists()) {
                    // Get the user profile from the result
                    order.userProfile = userProfileSnapshot.toObject(UserProfile::class.java)
                }

                return order
            }

            return null
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return null
        }
    }

    override suspend fun getNumberOfOrders(): Long {
        try {
            // Count the number of orders in Firestore database
            val snapshot = Firebase.firestore
                .collection(ORDERS_COLLECTION)
                .count()
                .get(AggregateSource.SERVER)
                .await()

            return snapshot.count
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            return 0
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        private const val LOG_TAG = "OrderRepository"

        // Define collection names
        private const val ORDERS_COLLECTION = "orders"
        private const val ORDER_ITEM_COLLECTION = "orderItems"
        private const val USERS_COLLECTION = "users"
        private const val PRODUCTS_COLLECTION = "products"
    }
}