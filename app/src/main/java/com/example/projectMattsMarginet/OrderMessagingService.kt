package com.example.projectMattsMarginet

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import com.example.projectMattsMarginet.models.UserProfile
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.google.firebase.firestore.firestore
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlin.random.Random

class OrderMessagingService: FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        if (message.notification != null) {
            // show notification if app is on foreground
            showNotification(message.notification!!.body, message.notification!!.title)
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        val uid = Firebase.auth.currentUser?.uid

        // Check if user is logged in
        if (uid.isNullOrBlank()) return

        GlobalScope.launch {
            // Get user profile from Firestore database
            val userProfileSnapshot = Firebase.firestore
                .collection(USERS_COLLECTION)
                .document(uid)
                .get()
                .await()

            // Check if user profile found
            if (userProfileSnapshot.exists()) {
                // Get user profile from result
                val userProfile = userProfileSnapshot
                    .toObject(UserProfile::class.java)

                // Check if token in user profile is different then new token
                if (userProfile!!.token != token) {
                    // Set token
                    userProfile.token = token

                    // Store user profile in Firestore database
                    Firebase.firestore
                        .collection(USERS_COLLECTION)
                        .document(uid)
                        .set(userProfile)
                        .await()
                }
            }
        }
    }

    @Suppress("SpellCheckingInspection")
    // See https://medium.com/@zikrira/android-notification-with-firebase-messaging-9b6b0dc1cf4f
    private fun showNotification(messageBody: String?, title: String?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        @Suppress("DEPRECATION")
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.icons8_ecommerce_64)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val channel = NotificationChannel(
            channelId,
            "Order Notifications",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(channel)
        notificationManager.notify(Random.nextInt(), notificationBuilder.build())
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "OrderMessagingService"

        // Define Users collection name
        private const val USERS_COLLECTION = "users"
    }
}