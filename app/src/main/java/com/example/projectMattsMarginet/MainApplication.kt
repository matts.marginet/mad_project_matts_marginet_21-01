package com.example.projectMattsMarginet

import android.app.Application
import android.widget.Toast
import androidx.room.Room
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.AuthRepositoryImpl
import com.example.projectMattsMarginet.repositories.BasketRepository
import com.example.projectMattsMarginet.repositories.BasketRepositoryImpl
import com.example.projectMattsMarginet.repositories.OrderRepository
import com.example.projectMattsMarginet.repositories.OrderRepositoryImpl
import com.example.projectMattsMarginet.repositories.ProductRepository
import com.example.projectMattsMarginet.repositories.ProductRepositoryImpl
import com.example.projectMattsMarginet.room.BasketDatabase
import com.google.firebase.FirebaseApp


class MainApplication : Application() {
    private lateinit var basketDatabase: BasketDatabase

    lateinit var authRepository: AuthRepository
        private set
    lateinit var productRepository: ProductRepository
        private set
    lateinit var basketRepository: BasketRepository
        private set
    lateinit var orderRepository: OrderRepository
        private set

    override fun onCreate() {
        super.onCreate()

        // Initialize Firebase app to use messaging
        FirebaseApp.initializeApp(this)

        basketDatabase = Room.databaseBuilder(
            this,
            BasketDatabase::class.java,
            "basket_database"
        ).build()

        authRepository = AuthRepositoryImpl()
        productRepository = ProductRepositoryImpl(this.applicationContext)
        basketRepository = BasketRepositoryImpl(basketDatabase.basketItemDao())
        orderRepository = OrderRepositoryImpl()
    }

    fun showShortToastMessage(message: String) {
        Toast.makeText(
            this,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }
}
