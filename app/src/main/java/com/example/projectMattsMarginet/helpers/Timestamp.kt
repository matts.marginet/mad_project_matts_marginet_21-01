package com.example.projectMattsMarginet.helpers

import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

// Helper to convert Firebase Timestamp to string
fun Timestamp.format(pattern: String): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.seconds * 1000
    val date = calendar.time
    val dateFormat = SimpleDateFormat(pattern, Locale.ENGLISH)
    return dateFormat.format(date)
}
