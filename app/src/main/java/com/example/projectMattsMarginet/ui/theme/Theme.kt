package com.example.projectMattsMarginet.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val DarkColorScheme = darkColorScheme(
    primary = DarkGrey,
    onPrimary = Color.White,

    secondaryContainer = DarkGrey,
    onSecondaryContainer = Color.White,

    secondary = Color.White,
    onSecondary = DarkGrey,

    tertiary = Color.White,
    onTertiary = DarkGrey,

    background = Color.Black,
    onBackground = Color.White,

    onError = Color.Red
)

private val LightColorScheme = lightColorScheme(
    primary = MiddelBlue,
    onPrimary = Color.White,

    secondaryContainer = Color.White,
    onSecondaryContainer = Color.Black,

    secondary = MiddelBlue,
    onSecondary = Color.White,

    tertiary = MiddelBlue,
    onTertiary = Color.White,

    background = Color.White,
    onBackground = Color.Black,

    onError = Color.Red
)

@Composable
fun ProjectMattsMarginetTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = false,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}