package com.example.projectMattsMarginet.ui.theme

import androidx.compose.ui.graphics.Color

//val LigtBlue = Color(0xFFb5e0e9)
val MiddelBlue = Color(0xFF0B52BA)
//val DarkBlue = Color(0xFF505E80)
val DarkGrey = Color(0xFF444852)
//val LigtBlack = Color(0xFF1E2638)

val White = Color(0xFFFFFFFF)
val Black = Color(0xFF111111)
//val grey = Color(0xFFE2E2E2)
