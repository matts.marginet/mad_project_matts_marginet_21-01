package com.example.projectMattsMarginet.screens.productAddOrEdit

import android.Manifest
import android.content.pm.PackageManager
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.R
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.components.ErrorMessage

import java.io.File

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductAddOrEditScreen(
    navController: NavHostController,
    productAddOrEditViewModel: ProductAddOrEditViewModel = viewModel(factory = ProductAddOrEditViewModel.Factory)
) {
    val context = LocalContext.current
    val mainApplication = LocalContext.current.applicationContext as MainApplication

    val product by productAddOrEditViewModel.product.collectAsState()
    val errorMessage by productAddOrEditViewModel.errorMessage.collectAsState()

    var hasCamPermission by remember {
        mutableStateOf(
            ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        )
    }
    val permissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { granted ->
            hasCamPermission = granted
        }
    )
    LaunchedEffect(key1 = true) {
        permissionLauncher.launch(Manifest.permission.CAMERA)
    }

    val pictureLauncher = rememberLauncherForActivityResult(ActivityResultContracts.TakePicturePreview()) { bitmap ->
        productAddOrEditViewModel.onPictureBitmapChange(bitmap)
    }

    Column {
        ErrorMessage(errorMessage)
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 20.dp, bottom = 5.dp)
                .padding(horizontal = 40.dp),
            contentAlignment = Alignment.Center
        ) {
            Column {
                OutlinedTextField(
                    value = product.title,
                    onValueChange = { newTitle ->
                        productAddOrEditViewModel.onTitleChange(newTitle)
                    },
                    label = {
                        Text(text = "Title")
                    },
                    isError = errorMessage.isNotBlank()
                )
                OutlinedTextField(
                    value = if (product.price == 0F) "" else product.price.toString(),
                    onValueChange = { newPriceString ->
                        val newPrice = newPriceString.toFloatOrNull()
                        productAddOrEditViewModel.onPriceChange(newPrice ?: 0F)
                    },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    label = {
                        Text(text = "Price")
                    },
                    isError = errorMessage.isNotBlank()
                )
                OutlinedTextField(
                    value = product.description,
                    onValueChange = { newDescription ->
                        productAddOrEditViewModel.onDescriptionChange(newDescription)
                    },
                    label = {
                        Text(text = "Description")
                    },
                    isError = errorMessage.isNotBlank()
                )
                Row {
                    AsyncImage(
                        modifier = Modifier
                            .size(150.dp)
                            .padding(top = 10.dp, end = 10.dp, bottom = 10.dp)
                            .border(1.dp, MaterialTheme.colorScheme.onBackground),
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(File(LocalContext.current.cacheDir, product.pictureFileName).absolutePath)
                            .build(),
                        contentDescription = product.title
                    )
                    Button(
                        modifier = Modifier.align(Alignment.Bottom),
                        onClick = {
                            if (hasCamPermission) {
                                pictureLauncher.launch()
                            }
                        },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.secondary,
                            contentColor = MaterialTheme.colorScheme.onSecondary
                        )
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.camera),
                            contentDescription = "Take picture",
                        )
                    }
                }
                Row {
                    Text(
                        modifier = Modifier.padding(vertical = 15.dp),
                        text = "In Stock"
                    )
                    Checkbox(
                        checked = product.inStock,
                        onCheckedChange = { newInStock ->
                            productAddOrEditViewModel.onInStockChange(newInStock)
                        }
                    )
                }
                Button(
                    modifier = Modifier.width(280.dp),
                    onClick = {
                        productAddOrEditViewModel.saveProduct {
                            mainApplication.showShortToastMessage("Product added successfully")
                            navController.navigate(Screen.Product.List.route)
                        }
                    },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.secondary,
                        contentColor = MaterialTheme.colorScheme.onSecondary
                    )
                ) {
                    Text(text = "Save product")
                }
            }
        }
    }
}