package com.example.projectMattsMarginet.screens.productDetail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.models.UserProfile
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.ProductRepository
import kotlinx.coroutines.launch

class ProductDetailViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val productRepository: ProductRepository
) : ViewModel() {
    private val productId: String = checkNotNull(savedStateHandle[PRODUCT_ID_KEY])

    val userProfile = savedStateHandle.getStateFlow<UserProfile?>(USER_PROFILE_KEY, null)
    val product = savedStateHandle.getStateFlow(PRODUCT_KEY, Product())

    init {
        viewModelScope.launch {
            savedStateHandle[USER_PROFILE_KEY] = authRepository.getCurrentUserProfile()
            savedStateHandle[PRODUCT_KEY] = productRepository.getProduct(productId) ?: Product()
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "ProductDetailViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val PRODUCT_ID_KEY = "productId"
        private const val PRODUCT_KEY = "product"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                ProductDetailViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    productRepository = mainApplication.productRepository
                )
            }
        }
    }
}