package com.example.projectMattsMarginet.screens.statistics

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.OrderRepository
import com.example.projectMattsMarginet.repositories.ProductRepository
import kotlinx.coroutines.launch

class StatisticsViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val productRepository: ProductRepository,
    private val orderRepository: OrderRepository
) : ViewModel() {
    val orderCount = savedStateHandle.getStateFlow(ORDER_COUNT_KEY, 0)
    val userCount = savedStateHandle.getStateFlow(USER_COUNT_KEY, 0)
    val productCount = savedStateHandle.getStateFlow(PRODUCT_COUNT_KEY, 0)

    init {
        viewModelScope.launch {
            savedStateHandle[ORDER_COUNT_KEY] = orderRepository.getNumberOfOrders()
            savedStateHandle[USER_COUNT_KEY] = authRepository.getNumberOfUsers()
            savedStateHandle[PRODUCT_COUNT_KEY] = productRepository.getNumberOfProducts()
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "HomeViewModel"

        // Define save state handle key's
        private const val ORDER_COUNT_KEY = "orderCount"
        private const val USER_COUNT_KEY = "userCount"
        private const val PRODUCT_COUNT_KEY = "productCount"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                StatisticsViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    productRepository = mainApplication.productRepository,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}