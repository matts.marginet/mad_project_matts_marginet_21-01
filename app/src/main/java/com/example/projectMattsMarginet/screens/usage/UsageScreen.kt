package com.example.projectMattsMarginet.screens.usage

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun UsageScreen() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp),
    ) {
        Column {
            Text(
                text = "User manual",
                fontSize = 22.sp,
                fontWeight = FontWeight.Medium
            )
            Spacer(modifier = Modifier.size(5.dp))
            Text(
                text = "After opening the app, you can register on the profile screen with a valid email address and create a user account. Subsequently, on the product screen, you can browse the list of available items to find something you want to borrow. Click on the desired item and reserve it by adding it to your shopping cart and completing the order. You will receive a notification when you can pick up your articles. Adhere to the agreed-upon term, which is standard set at 2 weeks. If you wish to alter this, you must discuss it with the owner. Return the borrowed item to the owner.",
                fontSize = 20.sp,
                textAlign = TextAlign.Justify
            )
        }
    }
}