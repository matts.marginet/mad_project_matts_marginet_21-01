package com.example.projectMattsMarginet.screens.productList

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.models.UserProfile
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.BasketRepository
import com.example.projectMattsMarginet.repositories.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductListViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val productRepository: ProductRepository,
    private val basketRepository: BasketRepository
) : ViewModel() {
    val userProfile = savedStateHandle.getStateFlow<UserProfile?>(USER_PROFILE_KEY, null)
    val productList = savedStateHandle.getStateFlow(PRODUCT_LIST_KEY, emptyList<Product>())

    init {
        viewModelScope.launch {
            savedStateHandle[USER_PROFILE_KEY] = authRepository.getCurrentUserProfile()
            savedStateHandle[PRODUCT_LIST_KEY] = productRepository.getProductList(userProfile.value)
        }
    }

    fun addProductToBasket(productId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            basketRepository.addProductToBasket(productId)
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "ProductListViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val PRODUCT_LIST_KEY = "productList"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                ProductListViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    productRepository = mainApplication.productRepository,
                    basketRepository = mainApplication.basketRepository
                )
            }
        }
    }
}