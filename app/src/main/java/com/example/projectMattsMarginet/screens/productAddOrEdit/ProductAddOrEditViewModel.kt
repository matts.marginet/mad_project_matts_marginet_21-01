package com.example.projectMattsMarginet.screens.productAddOrEdit

import android.graphics.Bitmap
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.repositories.ProductRepository
import kotlinx.coroutines.launch

class ProductAddOrEditViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val productRepository: ProductRepository
): ViewModel() {
    private val productId: String = checkNotNull(savedStateHandle[PRODUCT_ID_KEY])

    val product = savedStateHandle.getStateFlow(PRODUCT_KEY, Product())
    val errorMessage = savedStateHandle.getStateFlow(ERROR_MESSAGE_KEY, "")

    init {
        if (productId.isNotBlank() && productId != "add") {
            viewModelScope.launch {
                savedStateHandle[PRODUCT_KEY] = productRepository.getProduct(productId) ?: Product()
            }
        }
    }

    fun onPictureBitmapChange(picture: Bitmap?) {
        if (picture != null){
            viewModelScope.launch {
                val result = productRepository.savePicture(picture)
                if (result.isNotBlank()) {
                    if (result.endsWith(".jpg", ignoreCase = true)) {
                        savedStateHandle[PRODUCT_KEY] = product.value.copy(pictureFileName = result)
                    } else {
                        savedStateHandle[ERROR_MESSAGE_KEY] = result
                    }
                }
            }
        }
    }

    fun onTitleChange(title: String) {
        savedStateHandle[PRODUCT_KEY] = product.value.copy(title = title)
    }

    fun onPriceChange(price: Float) {
        savedStateHandle[PRODUCT_KEY] = product.value.copy(price = price)
    }

    fun onDescriptionChange(description: String) {
        savedStateHandle[PRODUCT_KEY] = product.value.copy(description = description)
    }

    fun onInStockChange(inStock: Boolean) {
        savedStateHandle[PRODUCT_KEY] = product.value.copy(inStock = inStock)
    }

    fun saveProduct(
        onNavigate: () -> Unit
    ) {
        if(product.value.title.isBlank() || product.value.pictureFileName.isBlank()) {
            savedStateHandle[ERROR_MESSAGE_KEY] = "Title and Picture are required"
            return
        }

        viewModelScope.launch {
            val errorMessage = productRepository.saveProduct(product.value)
            if (errorMessage.isBlank()) {
                savedStateHandle[PRODUCT_KEY] = Product()
                onNavigate.invoke()
            }

            savedStateHandle[ERROR_MESSAGE_KEY] = errorMessage
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "ProductAddOrEditViewModel"

        // Define save state handle key's
        private const val PRODUCT_ID_KEY = "productId"
        private const val PRODUCT_KEY = "product"
        private const val ERROR_MESSAGE_KEY = "errorMessage"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                ProductAddOrEditViewModel(
                    savedStateHandle = savedStateHandle,
                    productRepository = mainApplication.productRepository
                )
            }
        }
    }
}