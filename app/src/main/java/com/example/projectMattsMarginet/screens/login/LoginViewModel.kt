package com.example.projectMattsMarginet.screens.login

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.repositories.AuthRepository
import kotlinx.coroutines.launch

class LoginViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository
) : ViewModel() {
    val email = savedStateHandle.getStateFlow(EMAIL_KEY, "")
    val password = savedStateHandle.getStateFlow(PASSWORD_KEY, "")
    val errorMessage = savedStateHandle.getStateFlow(ERROR_MESSAGE_KEY, "")

    fun onEmailChange(email: String) {
        savedStateHandle[EMAIL_KEY] = email.trim()
    }

    fun onPasswordChange(password: String) {
        savedStateHandle[PASSWORD_KEY] = password.trim()
    }

    fun login( onNavigate: () -> Unit )  {
        savedStateHandle[ERROR_MESSAGE_KEY] = ""

        if(email.value.isBlank() || password.value.isBlank()) {
            savedStateHandle[ERROR_MESSAGE_KEY] = "Email and Password are required"
            return
        }

        viewModelScope.launch {
            val errorMessage = authRepository.login(email.value, password.value)
            if (errorMessage.isBlank()) {
                savedStateHandle[EMAIL_KEY] = ""
                savedStateHandle[PASSWORD_KEY] = ""
                savedStateHandle[ERROR_MESSAGE_KEY] = ""
                onNavigate.invoke()
            }

            savedStateHandle[ERROR_MESSAGE_KEY] = errorMessage
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "LoginViewModel"

        // Define save state handle key's
        private const val EMAIL_KEY = "email"
        private const val PASSWORD_KEY = "password"
        private const val ERROR_MESSAGE_KEY = "errorMessage"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                LoginViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository
                )
            }
        }
    }
}