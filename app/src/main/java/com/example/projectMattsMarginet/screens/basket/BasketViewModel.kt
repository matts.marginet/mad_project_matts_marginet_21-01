package com.example.projectMattsMarginet.screens.basket

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.OrderItem
import com.example.projectMattsMarginet.models.Product
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.BasketRepository
import com.example.projectMattsMarginet.repositories.OrderRepository
import com.example.projectMattsMarginet.repositories.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BasketViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val productRepository: ProductRepository,
    private val basketRepository: BasketRepository,
    private val orderRepository: OrderRepository
): ViewModel() {
    val productList = savedStateHandle.getStateFlow(PRODUCT_LIST_KEY, emptyList<Product>())
    val errorMessage = savedStateHandle.getStateFlow(ERROR_MESSAGE_KEY, "")

    init {
        viewModelScope.launch {
            savedStateHandle[USER_PROFILE_KEY] = authRepository.getCurrentUserProfile()
            refreshBasket()
        }
    }

    private suspend fun refreshBasket() {
        val productIdList = withContext(Dispatchers.IO) {
            basketRepository.getProductInBasketList()
        }
        savedStateHandle[PRODUCT_LIST_KEY] = productRepository.getProductByIdList(productIdList)
    }

    fun deleteProductFromBasket(productId: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                basketRepository.deleteProductFromBasket(productId)
            }
            refreshBasket()
        }
    }

    fun order() {
        viewModelScope.launch {
            savedStateHandle[ERROR_MESSAGE_KEY] = ""

            val errorList = mutableListOf<String>()
            productList.value.forEach {product ->
                val checkProduct = productRepository.getProduct(product.productId)
                if (checkProduct?.inStock == false) {
                    errorList.add("Product ${product.title} is out of stock")
                }
            }
            if (errorList.isNotEmpty()) {
                savedStateHandle[ERROR_MESSAGE_KEY] = errorList.joinToString()
                return@launch
            }

            val order = Order()
            productList.value.forEach {product ->
                product.inStock = false
                productRepository.saveProduct(product)

                order.orderItems.add(OrderItem(productId = product.productId))
            }

            val errorMessage = orderRepository.saveOrder(order)
            if (errorMessage.isBlank()) {
                withContext(Dispatchers.IO) {
                    basketRepository.clearBasket()
                }
                refreshBasket()
                return@launch
            }

            savedStateHandle[ERROR_MESSAGE_KEY] = errorMessage
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "BasketViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val PRODUCT_LIST_KEY = "productList"
        private const val ERROR_MESSAGE_KEY = "errorMessage"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as MainApplication
                BasketViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    productRepository = mainApplication.productRepository,
                    basketRepository = mainApplication.basketRepository,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}