package com.example.projectMattsMarginet.screens.statistics

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

@Composable
fun StatisticsScreen(
    statisticsViewModel: StatisticsViewModel = viewModel(factory = StatisticsViewModel.Factory)
) {
    val orderCount by statisticsViewModel.orderCount.collectAsState()
    val userCount by statisticsViewModel.userCount.collectAsState()
    val productCount by statisticsViewModel.productCount.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(5.dp)
    ){
        Column(
            modifier = Modifier
                .padding(30.dp)
        ){
            Text(
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                text = LocalDate
                    .now()
                    .format(
                        DateTimeFormatter.ofPattern("EEEE d MMMM y", Locale.ENGLISH)
                    )
            )
            Text(fontSize = 30.sp, fontWeight = FontWeight.Bold, text = "Overview of orders")
            Box(
                modifier = Modifier
                    .fillMaxWidth(),
                contentAlignment = Alignment.Center
            ){
                Column {
                    Column(
                        modifier = Modifier
                            .padding(vertical = 50.dp)
                            .size(150.dp)
                            .border(4.dp, MaterialTheme.colorScheme.primary, CircleShape)
                            .clip(CircleShape)
                            .background(MaterialTheme.colorScheme.background),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ){
                        Text(fontSize = 35.sp, fontWeight = FontWeight.Bold, text = orderCount.toString())

                    }
                }
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth(),
                contentAlignment = Alignment.Center
            ){
                Row {
                    Column {
                        Row {
                            Text(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold,
                                text = "Number of users",
                                modifier = Modifier.padding(vertical = 8.dp)
                            )
                            Text(
                                fontSize = 22.sp,
                                fontWeight = FontWeight.Bold,
                                text = userCount.toString(),
                                modifier = Modifier.padding(8.dp)
                            )
                        }
                        Row {
                            Text(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold,
                                text = "Number of products",
                                modifier = Modifier.padding(vertical = 8.dp)
                            )
                            Text(
                                fontSize = 22.sp,
                                fontWeight = FontWeight.Bold,
                                text = productCount.toString(),
                                modifier = Modifier.padding(8.dp)
                            )
                        }
                    }
                    Column {
                        Icon(
                            imageVector = Icons.Default.Person,
                            contentDescription = "Add",
                            modifier = Modifier
                                .padding(vertical = 6.5.dp, horizontal = 18.dp)
                                .size(30.dp),
                        )
                        Icon(
                            imageVector = Icons.Default.List,
                            contentDescription = "Add",
                            modifier = Modifier
                                .padding(vertical = 8.dp, horizontal = 18.dp)
                                .size(30.dp),
                        )
                    }
                }
            }
        }
    }
}