package com.example.projectMattsMarginet.screens.register

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.UserProfile
import com.example.projectMattsMarginet.repositories.AuthRepository
import kotlinx.coroutines.launch

class RegisterViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository
) : ViewModel() {
    val userProfile = savedStateHandle.getStateFlow(USER_PROFILE_KEY, UserProfile())
    val password = savedStateHandle.getStateFlow(PASSWORD_KEY, "")
    val passwordConfirmation = savedStateHandle.getStateFlow(PASSWORD_CONFIRMATION_KEY, "")
    val errorMessage = savedStateHandle.getStateFlow(ERROR_MESSAGE_KEY, "")

    fun onEmailChange(email: String) {
        savedStateHandle[USER_PROFILE_KEY] = userProfile.value.copy(email = email.trim())
    }

    fun onPhoneNumberChange(phoneNumber: String) {
        savedStateHandle[USER_PROFILE_KEY] = userProfile.value.copy(phoneNumber = phoneNumber.trim())
    }

    fun onPasswordChange(password: String) {
        savedStateHandle[PASSWORD_KEY] = password.trim()
    }

    fun onPasswordConfirmationChange(passwordConfirmation: String) {
        savedStateHandle[PASSWORD_CONFIRMATION_KEY] = passwordConfirmation.trim()
    }

    fun registerUser(onNavigate: () -> Unit) {
        savedStateHandle[ERROR_MESSAGE_KEY] = ""

        if(userProfile.value.email.isBlank() || userProfile.value.phoneNumber.isBlank() || password.value.isBlank() || passwordConfirmation.value.isBlank()) {
            savedStateHandle[ERROR_MESSAGE_KEY] = "Email, Phone Number, Password and Password Confirmation are required"
            return
        }

        if (password.value != passwordConfirmation.value) {
            savedStateHandle[ERROR_MESSAGE_KEY] = "Password and Password Confirmation are not equal"
            return
        }

        viewModelScope.launch {
            val errorMessage = authRepository.register(userProfile.value, password.value)
            if (errorMessage.isBlank()) {
                savedStateHandle[USER_PROFILE_KEY] = UserProfile()
                savedStateHandle[PASSWORD_KEY] = ""
                savedStateHandle[PASSWORD_CONFIRMATION_KEY] = ""
                savedStateHandle[ERROR_MESSAGE_KEY] = ""
                onNavigate.invoke()
            }

            savedStateHandle[ERROR_MESSAGE_KEY] = errorMessage
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "RegisterViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val PASSWORD_KEY = "password"
        private const val PASSWORD_CONFIRMATION_KEY = "passwordConfirmation"
        private const val ERROR_MESSAGE_KEY = "errorMessage"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                RegisterViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository
                )
            }
        }
    }
}