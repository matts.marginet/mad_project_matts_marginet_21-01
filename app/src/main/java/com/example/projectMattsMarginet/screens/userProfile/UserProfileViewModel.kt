package com.example.projectMattsMarginet.screens.userProfile

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.UserProfile
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.OrderRepository
import kotlinx.coroutines.launch

class UserProfileViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val orderRepository: OrderRepository
) : ViewModel() {
    val userProfile = savedStateHandle.getStateFlow(USER_PROFILE_KEY, UserProfile())
    val orderList = savedStateHandle.getStateFlow(ORDER_LIST_KEY, emptyList<Order>())

    init {
        viewModelScope.launch {
            val currentUserProfile = authRepository.getCurrentUserProfile()
            if (currentUserProfile != null) {
                savedStateHandle[USER_PROFILE_KEY] = currentUserProfile
            }
            savedStateHandle[ORDER_LIST_KEY] = orderRepository.getOrderListForProfile()
        }
    }

    fun logout (
        onNavigate: () -> Unit
    ) {
        authRepository.logout()
        onNavigate.invoke()
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "UserProfileViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val ORDER_LIST_KEY = "orderList"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[APPLICATION_KEY] as MainApplication
                UserProfileViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}