package com.example.projectMattsMarginet.screens.orderDatail

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun OrderDetailScreen(
    orderDetailViewModel: OrderDetailViewModel = viewModel(factory = OrderDetailViewModel.Factory)
) {
    val order by orderDetailViewModel.order.collectAsState()

    Box(
        modifier = Modifier
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
            .fillMaxSize(),
    ){
        Column {
            Row {
                Text(
                    modifier = Modifier
                        .padding(vertical = 4.dp),
                    fontSize = 22.sp,
                    fontWeight = FontWeight.Medium,
                    text = "Order number:"
                )
                Spacer(modifier = Modifier
                    .size(5.dp)
                )
                Text(
                    modifier = Modifier
                        .padding(vertical = 6.dp),
                    fontSize = 20.sp,
                    text = order.orderNumber
                        .toString()
                        .padStart(5, '0')
                )
            }
            Row {
                Text(
                    modifier = Modifier
                        .padding(vertical = 4.dp),
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Medium,
                    text = "Order status:"
                )
                Spacer(modifier = Modifier
                    .size(5.dp)
                )
                Text(
                    modifier = Modifier
                        .padding(vertical = 6.dp),
                    fontSize = 16.sp,
                    text = order.status.displayName
                )
            }
            LazyVerticalGrid(
                columns = GridCells.Fixed(1),
                modifier = Modifier
                    .padding(vertical = 15.dp)
                    .fillMaxWidth()
            ) {
                items(order.orderItems){ orderItem ->
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(140.dp)
                            .padding(vertical = 15.dp),
                        elevation = CardDefaults.cardElevation(10.dp),
                        colors = CardDefaults.cardColors(
                            containerColor = MaterialTheme.colorScheme.secondaryContainer,
                            contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    ) {
                        Column(
                            modifier = Modifier
                                .padding(15.dp),
                            verticalArrangement = Arrangement.Center
                        ) {
                            Row {
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 22.sp,
                                    fontWeight = FontWeight.Medium,
                                    text = "Product:"
                                )
                                Spacer(modifier = Modifier
                                    .size(5.dp)
                                )
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.Bold,
                                    text = orderItem.product?.title ?: ""
                                )
                            }
                            Row {
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 18.sp,
                                    fontWeight = FontWeight.Medium,
                                    text = "Status:"
                                )
                                Spacer(modifier = Modifier
                                    .size(5.dp)
                                )
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 16.sp,
                                    text = orderItem.status.displayName
                                )
                            }
                        }

                    }
                }
            }
        }
    }
}