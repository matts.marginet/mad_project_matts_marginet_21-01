package com.example.projectMattsMarginet.screens.productList

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.Screen
import java.io.File

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductListScreen(
    navController: NavHostController,
    productListViewModel: ProductListViewModel = viewModel(factory = ProductListViewModel.Factory)
) {
    val mainApplication = LocalContext.current.applicationContext as MainApplication

    val userProfile by productListViewModel.userProfile.collectAsState()
    val productList by productListViewModel.productList.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
    ){
        LazyVerticalGrid(
            columns = GridCells.Fixed(1),
            modifier = Modifier
                .fillMaxWidth()
        ){
            items(productList){ product ->
                Card(
                    onClick = { navController.navigate("${Screen.Product.Detail.route}/${product.productId}") },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(280.dp)
                        .padding(vertical = 15.dp),
                    elevation = CardDefaults.cardElevation(10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.secondaryContainer,
                        contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                ) {
                    Box(
                        modifier = Modifier.fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ){
                        AsyncImage(
                            modifier = Modifier
                                .size(150.dp)
                                .padding(15.dp),
                            model = ImageRequest.Builder(LocalContext.current)
                                .data(File(LocalContext.current.cacheDir, product.pictureFileName).absolutePath)
                                .build(),
                            contentDescription = product.title
                        )
                    }
                    Box(
                        modifier = Modifier.padding(horizontal = 15.dp)
                    ){
                        Column {
                            Row {
                                Text(
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.Bold,
                                    text = product.title,
                                    modifier = Modifier.padding(vertical = 4.dp)
                                )
                                Box(
                                    modifier = Modifier
                                        .fillMaxWidth(),
                                    contentAlignment = Alignment.BottomEnd
                                ){
                                    Button(
                                        onClick = {
                                            mainApplication.showShortToastMessage("Product added successfully to basket")
                                            productListViewModel.addProductToBasket(product.productId)
                                        },
                                        colors = ButtonDefaults.buttonColors(
                                            containerColor = MaterialTheme.colorScheme.secondary,
                                            contentColor = MaterialTheme.colorScheme.onSecondary
                                        )
                                    ) {
                                        Icon(
                                            imageVector = Icons.Default.ShoppingCart,
                                            contentDescription = "Add to shopping basket"
                                        )
                                    }
                                }
                            }
                            Text(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold,
                                text = if (product.inStock) "in stock" else "Out of Stock",
                                modifier = Modifier
                                    .padding(vertical = 11.dp),
                            )
                        }
                    }
                }
            }
        }
    }
    Box(
        Modifier.fillMaxSize()
    ){
        if (userProfile != null && userProfile!!.isAdmin) {
            FloatingActionButton(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(15.dp),
                onClick = { navController.navigate("${Screen.Product.AddOrEdit.route}/add") },
                containerColor = MaterialTheme.colorScheme.tertiary,
                contentColor = MaterialTheme.colorScheme.onTertiary
            ){
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Add"
                )
            }
        }
    }
}