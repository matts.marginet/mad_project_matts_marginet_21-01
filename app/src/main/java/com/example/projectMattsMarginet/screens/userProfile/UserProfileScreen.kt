package com.example.projectMattsMarginet.screens.userProfile

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.helpers.format
import com.example.projectMattsMarginet.models.Order


@Composable
fun ProfileScreen(
    navController: NavHostController,
    userProfileViewModel: UserProfileViewModel = viewModel(factory = UserProfileViewModel.Factory)
) {
    val userProfile by userProfileViewModel.userProfile.collectAsState()
    val orderList by userProfileViewModel.orderList.collectAsState()

    Box(
        modifier = Modifier
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
            .fillMaxSize(),
    ) {
        Column {
            Text(
                text = "My Profile",
                fontSize = 30.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(bottom = 20.dp)
            )

            Row {
                Text(
                    text = "Email:",
                    fontSize = 22.sp,
                    fontWeight = FontWeight.Medium
                )
                Spacer(modifier = Modifier.size(15.dp))
                Text(
                    text = userProfile.email,
                    fontSize = 20.sp
                )
            }
            Row {
                Text(
                    text = "Phone Number:",
                    fontSize = 22.sp,
                    fontWeight = FontWeight.Medium
                )
                Spacer(modifier = Modifier.size(15.dp))
                Text(
                    text = userProfile.phoneNumber,
                    fontSize = 20.sp
                )
            }
            OrderList(
                navController = navController,
                orderList = orderList
            )
        }
    }
    Box(
        Modifier.fillMaxSize()
    ){
        FloatingActionButton(
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(15.dp),
            onClick = {
                userProfileViewModel.logout {
                    navController.navigate(Screen.Home.route)
                }
            },
            containerColor = MaterialTheme.colorScheme.tertiary,
            contentColor = MaterialTheme.colorScheme.onTertiary
        ){
            Icon(
                imageVector = Icons.Default.ExitToApp,
                contentDescription = "Add"
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderList(
    navController: NavHostController,
    orderList: List<Order>
) {
    Column {
        Text(
            text = "Orders",
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 5.dp, top = 30.dp),
        )
        LazyVerticalGrid(
            columns = GridCells.Fixed(1),
            modifier = Modifier
                .fillMaxWidth()
        ){
            items(orderList){ order ->
                Card(
                    onClick = { navController.navigate("${Screen.Auth.OrderDetail.route}/${order.orderId}") },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(150.dp)
                        .padding(vertical = 15.dp),
                    elevation = CardDefaults.cardElevation(10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.secondaryContainer,
                        contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                ) {
                    Column(
                        modifier = Modifier.padding(10.dp),
                        verticalArrangement = Arrangement.Center
                    ){
                        Row(
                            modifier = Modifier.padding(horizontal = 5.dp),
                        ) {
                            Text(
                                text = "Order number:",
                                fontSize = 22.sp,
                                fontWeight = FontWeight.Medium,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                            Spacer(modifier = Modifier.size(15.dp))
                            Text(
                                text = order.orderNumber
                                    .toString()
                                    .padStart(5, '0'),
                                fontSize = 20.sp,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(horizontal = 5.dp),
                        ) {
                            Text(
                                text = "Created:",
                                fontSize = 18.sp,
                                fontWeight = FontWeight.Medium,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                            Spacer(modifier = Modifier.size(15.dp))
                            Text(
                                text = order.createdAt.format("MMM dd, yyyy HH:mm"),
                                fontSize = 16.sp,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(horizontal = 5.dp),
                        ) {
                            Text(
                                text = "Status:",
                                fontSize = 18.sp,
                                fontWeight = FontWeight.Medium,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                            Spacer(modifier = Modifier.size(15.dp))
                            Text(
                                text = order.status.displayName,
                                fontSize = 16.sp,
                                modifier = Modifier
                                    .padding(vertical = 4.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}