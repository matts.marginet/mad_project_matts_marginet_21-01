package com.example.projectMattsMarginet.screens.orderList

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.OrderStatus
import com.example.projectMattsMarginet.models.OrderStatusFilter
import com.example.projectMattsMarginet.models.UserProfile
import com.example.projectMattsMarginet.repositories.AuthRepository
import com.example.projectMattsMarginet.repositories.OrderRepository
import com.example.projectMattsMarginet.repositories.ProductRepository
import com.google.firebase.Timestamp
import kotlinx.coroutines.launch

class OrderListViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val authRepository: AuthRepository,
    private val productRepository: ProductRepository,
    private val orderRepository: OrderRepository,
) : ViewModel() {
    val userProfile = savedStateHandle.getStateFlow<UserProfile?>(USER_PROFILE_KEY, null)
    val orderStatusFilter = savedStateHandle.getStateFlow(ORDER_STATUS_FILTER_KEY, OrderStatus.values().map { orderStatus ->
        OrderStatusFilter(
            status = orderStatus,
            checked = orderStatus == OrderStatus.IN_PROGRESS || orderStatus == OrderStatus.READY
        )
    })
    val orderList = savedStateHandle.getStateFlow(ORDER_LIST_KEY, emptyList<Order>())

    init {
        viewModelScope.launch {
            savedStateHandle[USER_PROFILE_KEY] = authRepository.getCurrentUserProfile()
            refreshOrderList()
        }
    }

    fun onOrderStatusFilterChange(checked: Boolean, changedOrderStatus: OrderStatus) {
        val newList = orderStatusFilter.value.map { filter ->
            if (filter.status.name == changedOrderStatus.name) {
                filter.copy(checked = checked)
            } else {
                filter
            }
        }
        savedStateHandle[ORDER_STATUS_FILTER_KEY] = newList
        viewModelScope.launch {
            refreshOrderList()
        }
    }

    fun setOrderStatusPickedUp(orderId:String) {
        val order = orderList.value.first {order -> order.orderId == orderId}
        order.status = OrderStatus.PICKED_UP
        order.pickedUpAt = Timestamp.now()
        viewModelScope.launch {
            orderRepository.saveOrder(order)
            refreshOrderList()
        }
    }

    fun setOrderStatusReturned(orderId:String) {
        viewModelScope.launch {
            val order = orderRepository.getOrder(orderId)
            if (order != null) {
                order.status = OrderStatus.RETURNED
                order.returnedAt = Timestamp.now()
                orderRepository.saveOrder(order)

                refreshOrderList()

                val productIdList = order.orderItems.map {orderItem ->
                    orderItem.productId
                }
                productRepository.setProductsInStock(productIdList)
            }
        }
    }

    private suspend fun refreshOrderList() {
        if (userProfile.value != null && userProfile.value!!.isAdmin) {
            savedStateHandle[ORDER_LIST_KEY] = orderRepository.getOrderListForAdmin(orderStatusFilter.value)
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "OrderListViewModel"

        // Define save state handle key's
        private const val USER_PROFILE_KEY = "userProfile"
        private const val ORDER_STATUS_FILTER_KEY = "orderStatusFilter"
        private const val ORDER_LIST_KEY = "orderList"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as MainApplication
                OrderListViewModel(
                    savedStateHandle = savedStateHandle,
                    authRepository = mainApplication.authRepository,
                    productRepository = mainApplication.productRepository,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}