package com.example.projectMattsMarginet.screens.orderList

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.projectMattsMarginet.R
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.helpers.format
import com.example.projectMattsMarginet.models.OrderStatus

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderListScreen(
    navController: NavHostController,
    orderListViewModel: OrderListViewModel = viewModel(factory = OrderListViewModel.Factory)
) {
    val userProfile by orderListViewModel.userProfile.collectAsState()
    val orderStatusFilter by orderListViewModel.orderStatusFilter.collectAsState()
    val orderList by orderListViewModel.orderList.collectAsState()

    Box(
        modifier = Modifier
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
            .fillMaxSize(),
    ){
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row {
                orderStatusFilter.forEach {filter ->
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(horizontal = 8.dp),
                            fontSize = 15.sp,
                            text = filter.status.displayName
                        )
                        Checkbox(
                            checked = filter.checked,
                            onCheckedChange = {newChecked ->
                                orderListViewModel.onOrderStatusFilterChange(newChecked, filter.status)
                            }
                        )
                    }
                }
            }
            LazyVerticalGrid(
                columns = GridCells.Fixed(1),
                modifier = Modifier
                    .fillMaxWidth()
            ){
                items(orderList){ order ->
                    Card(
                        onClick = { navController.navigate("${Screen.Home.OrderPicking.route}/${order.orderId}") },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(190.dp)
                            .padding(vertical = 15.dp),
                        elevation = CardDefaults.cardElevation(10.dp),
                        colors = CardDefaults.cardColors(
                            containerColor = MaterialTheme.colorScheme.secondaryContainer,
                            contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    ) {
                        Column(
                            modifier = Modifier
                                .padding(horizontal = 10.dp),
                            verticalArrangement = Arrangement.Center
                        ){
                            Row {
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 22.sp,
                                    text = "Order number:",
                                    fontWeight = FontWeight.Medium
                                )
                                Spacer(modifier = Modifier
                                    .size(5.dp)
                                )
                                Text(
                                    modifier = Modifier
                                        .padding(vertical = 4.dp),
                                    fontSize = 20.sp,
                                    text = order.orderNumber
                                        .toString()
                                        .padStart(5, '0')
                                )
                            }
                            Row {
                                Column {
                                    Row {
                                        Text(
                                            modifier = Modifier
                                                .padding(vertical = 4.dp),
                                            fontSize = 18.sp,
                                            text = "Created:",
                                            fontWeight = FontWeight.Medium
                                        )
                                        Spacer(modifier = Modifier
                                            .size(5.dp)
                                        )
                                        Text(
                                            modifier = Modifier
                                                .padding(vertical = 4.dp),
                                            fontSize = 16.sp,
                                            text = order.createdAt.format("MMM dd, yyyy HH:mm")
                                        )
                                    }
                                    Row {
                                        Text(
                                            modifier = Modifier
                                                .padding(vertical = 4.dp),
                                            fontSize = 18.sp,
                                            text = "Status:",
                                            fontWeight = FontWeight.Medium
                                        )
                                        Spacer(modifier = Modifier
                                            .size(5.dp)
                                        )
                                        Text(
                                            modifier = Modifier
                                                .padding(vertical = 4.dp),
                                            fontSize = 16.sp,
                                            text = order.status.displayName
                                        )
                                    }
                                }
                            }
                            if (order.status == OrderStatus.READY) {
                                Button(
                                    onClick = {
                                        orderListViewModel.setOrderStatusPickedUp(order.orderId)
                                    },
                                    colors = ButtonDefaults.buttonColors(
                                        containerColor = MaterialTheme.colorScheme.secondary,
                                        contentColor = MaterialTheme.colorScheme.onSecondary
                                    )
                                ) {
                                    Text(text = "Picked up")
                                }
                            }
                            if (order.status == OrderStatus.PICKED_UP) {
                                Button(
                                    onClick = {
                                        orderListViewModel.setOrderStatusReturned(order.orderId)
                                    },
                                    colors = ButtonDefaults.buttonColors(
                                        containerColor = MaterialTheme.colorScheme.secondary,
                                        contentColor = MaterialTheme.colorScheme.onSecondary
                                    )
                                ) {
                                    Text(text = "Returned")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Box(
        Modifier.fillMaxSize()
    ){
        if (userProfile != null && userProfile!!.isAdmin) {
            FloatingActionButton(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(15.dp),
                onClick = { navController.navigate(Screen.Home.Statistics.route) },
                containerColor = MaterialTheme.colorScheme.tertiary,
                contentColor = MaterialTheme.colorScheme.onTertiary
            ){
                Icon(
                    painter = painterResource(id = R.drawable.chart_bar),
                    contentDescription = "Statistics"
                )
            }
        }
    }
}