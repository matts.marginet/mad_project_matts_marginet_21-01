package com.example.projectMattsMarginet.screens.orderPicking

import android.Manifest
import android.content.pm.PackageManager
import android.util.Size
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.R
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.helpers.QrCodeAnalyzer
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.OrderStatus

@Composable
fun OrderPickingScreen(
    navController: NavHostController,
    orderPickingViewModel: OrderPickingViewModel = viewModel(factory = OrderPickingViewModel.Factory)
) {
    val mainApplication = LocalContext.current.applicationContext as MainApplication
    val order by orderPickingViewModel.order.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 20.dp, bottom = 5.dp)
            .padding(horizontal = 40.dp)
    ){
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            QrCodeScanner(
                mainApplication = mainApplication,
                orderPickingViewModel = orderPickingViewModel
            )
            OrderDetail(order = order)
        }
    }
    Box(
        Modifier.fillMaxSize()
    ){
        if (order.status == OrderStatus.READY){
            FloatingActionButton(
                modifier = Modifier
                    .align(Alignment.TopEnd)
                    .padding(15.dp),
                onClick = {
                    orderPickingViewModel.save()
                    mainApplication.showShortToastMessage("Order successfully saved")
                    navController.navigate(Screen.Home.route)
                },
                containerColor = MaterialTheme.colorScheme.tertiary,
                contentColor = MaterialTheme.colorScheme.onTertiary
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.content_save),
                    contentDescription = "Statistics"
                )
            }
        }
    }
}

@Composable
fun OrderDetail(
    order: Order
) {
    Column {
        Row {
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 22.sp,
                fontWeight = FontWeight.Medium,
                text = "Order number:"
            )
            Spacer(
                modifier = Modifier
                    .size(5.dp)
            )
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 20.sp,
                text = order.orderNumber
                    .toString()
                    .padStart(5, '0')
            )
        }
        Row {
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 18.sp,
                fontWeight = FontWeight.Medium,
                text = "Email:"
            )
            Spacer(
                modifier = Modifier
                    .size(5.dp)
            )
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 16.sp,
                text = order.userProfile?.email ?: ""
            )
        }
        Row {
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 18.sp,
                fontWeight = FontWeight.Medium,
                text = "Order status:"
            )
            Spacer(modifier = Modifier
                .size(5.dp)
            )
            Text(
                modifier = Modifier
                    .padding(vertical = 4.dp),
                fontSize = 16.sp,
                text = order.status.displayName
            )
        }
        LazyVerticalGrid(
            columns = GridCells.Fixed(1),
            modifier = Modifier
                .fillMaxWidth()
        ) {
            items(order.orderItems){ orderItem ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(120.dp)
                        .padding(vertical = 15.dp),
                    elevation = CardDefaults.cardElevation(10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.secondaryContainer,
                        contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                ) {
                    Column(
                        modifier = Modifier.padding(horizontal = 15.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Row {
                            Text(
                                modifier = Modifier
                                    .padding(vertical = 4.dp),
                                fontSize = 22.sp,
                                text = "Product:",
                                fontWeight = FontWeight.Medium
                            )
                            Spacer(modifier = Modifier
                                .size(5.dp)
                            )
                            Text(
                                modifier = Modifier
                                    .padding(vertical = 4.dp),
                                fontSize = 20.sp,
                                text = orderItem.product?.title ?: ""
                            )
                        }
                        Row {
                            Text(
                                modifier = Modifier
                                    .padding(vertical = 4.dp),
                                fontSize = 18.sp,
                                text = "Status:",
                                fontWeight = FontWeight.Medium
                            )
                            Spacer(modifier = Modifier
                                .size(5.dp)
                            )
                            Text(
                                modifier = Modifier
                                    .padding(vertical = 4.dp),
                                fontSize = 16.sp,
                                text = orderItem.status.displayName
                            )
                        }
                    }

                }
            }
        }
    }
}

@Composable
fun QrCodeScanner(
    mainApplication: MainApplication,
    orderPickingViewModel: OrderPickingViewModel
) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val cameraProviderFuture = remember {
        ProcessCameraProvider.getInstance(context)
    }
    var hasCamPermission by remember {
        mutableStateOf(
            ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        )
    }
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { granted ->
            hasCamPermission = granted
        }
    )
    LaunchedEffect(key1 = true) {
        launcher.launch(Manifest.permission.CAMERA)
    }

    if (hasCamPermission) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(240.dp)
                .clipToBounds()
        ) {
            AndroidView(
                modifier = Modifier
                    .matchParentSize()
                    .align(Alignment.Center),
                factory = { context ->
                    val previewView = PreviewView(context)
                    val preview = Preview.Builder().build()
                    val selector = CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                        .build()
                    preview.setSurfaceProvider(previewView.surfaceProvider)
                    val imageAnalysis = ImageAnalysis.Builder()
                        .setTargetResolution(
                            Size(
                                previewView.width,
                                previewView.height
                            )
                        )
                        .setBackpressureStrategy(STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                    imageAnalysis.setAnalyzer(
                        ContextCompat.getMainExecutor(context),
                        QrCodeAnalyzer { productId ->
                            if (orderPickingViewModel.onProductIdChange(productId)) {
                                mainApplication.showShortToastMessage("Product successfully picked")
                            }
                        }
                    )
                    try {
                        cameraProviderFuture.get().bindToLifecycle(
                            lifecycleOwner,
                            selector,
                            preview,
                            imageAnalysis
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    previewView
                }
            )
        }
    }
}