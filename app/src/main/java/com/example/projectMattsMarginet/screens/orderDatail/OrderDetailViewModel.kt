package com.example.projectMattsMarginet.screens.orderDatail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.repositories.OrderRepository
import kotlinx.coroutines.launch

class OrderDetailViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val orderRepository: OrderRepository
) : ViewModel() {
    private val orderId: String = checkNotNull(savedStateHandle[ORDER_ID_KEY])

    val order = savedStateHandle.getStateFlow(ORDER_KEY, Order())

    init {
        viewModelScope.launch {
            savedStateHandle[ORDER_KEY] = orderRepository.getOrder(orderId) ?: Order()
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "OrderListViewModel"

        // Define save state handle key's
        private const val ORDER_ID_KEY = "orderId"
        private const val ORDER_KEY = "order"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as MainApplication
                OrderDetailViewModel(
                    savedStateHandle = savedStateHandle,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}