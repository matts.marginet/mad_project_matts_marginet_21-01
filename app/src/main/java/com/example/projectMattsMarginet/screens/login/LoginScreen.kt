package com.example.projectMattsMarginet.screens.login

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.components.ErrorMessage


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginScreen(
    navController: NavHostController,
    loginViewModel: LoginViewModel = viewModel(factory = LoginViewModel.Factory)
) {
    val email by loginViewModel.email.collectAsState()
    val password by loginViewModel.password.collectAsState()
    val errorMessage by loginViewModel.errorMessage.collectAsState()

    Box(
        modifier = Modifier
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ){
        Column {
            ErrorMessage(errorMessage)
            Box{
                Column {
                    OutlinedTextField(
                        value = email,
                        onValueChange = { newEmail ->
                            loginViewModel.onEmailChange(newEmail)
                        },
                        label = {
                            Text(text = "Email")
                        },
                        isError = errorMessage.isNotBlank()
                    )
                    OutlinedTextField(
                        value = password,
                        onValueChange = { newPassword ->
                            loginViewModel.onPasswordChange(newPassword)
                        },
                        label = {
                            Text(text = "Password")
                        },
                        visualTransformation = PasswordVisualTransformation(),
                        isError = errorMessage.isNotBlank()
                    )
                    Button(
                        onClick = {
                            loginViewModel.login {
                                navController.navigate(Screen.Auth.Profile.route)
                            }
                        },
                        modifier = Modifier
                            .width(280.dp)
                            .padding(vertical = 15.dp),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.secondary,
                            contentColor = MaterialTheme.colorScheme.onSecondary
                        )
                    ) {
                        Text(text = "Login")
                    }
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 5.dp),
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "Don't have an Account?",
                            fontSize = 16.sp
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                        TextButton(
                            onClick = { navController.navigate(Screen.Auth.Register.route) },
                            colors = ButtonDefaults.buttonColors(
                                containerColor = Color.Transparent,
                                contentColor = MaterialTheme.colorScheme.primary
                            )
                        ) {
                            Text(
                                text = "Register",
                                fontSize = 18.sp,
                                fontWeight = FontWeight.Bold
                            )
                        }
                    }
                }
            }
        }
    }

}