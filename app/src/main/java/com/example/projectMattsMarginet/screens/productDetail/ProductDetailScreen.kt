package com.example.projectMattsMarginet.screens.productDetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Create
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.helpers.rememberQrBitmapPainter
import java.io.File

@Composable
fun ProductDetailScreen(
    navController: NavHostController,
    productDetailViewModel: ProductDetailViewModel = viewModel(factory = ProductDetailViewModel.Factory)
) {
    val userProfile by productDetailViewModel.userProfile.collectAsState()
    val product by productDetailViewModel.product.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 20.dp)
            .padding(horizontal = 40.dp)
    ){
        Column {
            Text(fontSize = 30.sp, fontWeight = FontWeight.Bold, text = product.title)
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(50.dp),
                contentAlignment = Alignment.Center
            ){
                AsyncImage(
                    modifier = Modifier
                        .size(200.dp)
                        .padding(10.dp),
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(File(LocalContext.current.cacheDir, product.pictureFileName).absolutePath)
                        .build(),
                    contentDescription = product.title
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth(),
                contentAlignment = Alignment.Center
            ){
                Column {
                    Row {
                        Text(
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Medium,
                            text = "Price:"
                        )
                        Spacer(modifier = Modifier
                            .size(5.dp)
                        )
                        Text(
                            fontSize = 20.sp,
                            text = product.price.toString(),
                        )
                    }
                    Row {
                        Text(
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Medium,
                            text = "Description:",
                        )
                        Spacer(modifier = Modifier
                            .size(5.dp)
                        )
                        Text(
                            fontSize = 20.sp,
                            text = product.description,
                        )
                    }
                    Row {
                        Text(
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Medium,
                            text = if (product.inStock) "in stock" else "Out of Stock",
                            modifier = Modifier.padding(vertical = 10.dp),
                        )
                    }
                    if (product.productId.isNotBlank()
                        && userProfile != null
                        && userProfile!!.isAdmin) {
                        Spacer(modifier = Modifier.size(10.dp))
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Image(
                                painter = rememberQrBitmapPainter(product.productId),
                                contentDescription = "Product QR Code",
                                contentScale = ContentScale.FillBounds,
                                modifier = Modifier
                                    .size(160.dp)
                                    .border(10.dp, Color.White)
                                    .padding(10.dp),
                            )
                        }
                    }
                }
            }
        }
    }
    Box(
        Modifier.fillMaxSize()
    ){
        if (userProfile != null && userProfile!!.isAdmin) {
            FloatingActionButton(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(15.dp),
                onClick = { navController.navigate("${Screen.Product.AddOrEdit.route}/${product.productId}") },
                containerColor = MaterialTheme.colorScheme.tertiary,
                contentColor = MaterialTheme.colorScheme.onTertiary
            ){
                Icon(
                    imageVector = Icons.Default.Create,
                    contentDescription = "Edit"
                )
            }
        }
    }
}