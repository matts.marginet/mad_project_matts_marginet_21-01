package com.example.projectMattsMarginet.screens.orderPicking

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.projectMattsMarginet.MainApplication
import com.example.projectMattsMarginet.models.Order
import com.example.projectMattsMarginet.models.OrderItemStatus
import com.example.projectMattsMarginet.models.OrderStatus
import com.example.projectMattsMarginet.repositories.OrderRepository
import kotlinx.coroutines.launch
import com.google.firebase.Timestamp

class OrderPickingViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val orderRepository: OrderRepository
): ViewModel() {
    private val orderId: String = checkNotNull(savedStateHandle[ORDER_ID_KEY])

    val order = savedStateHandle.getStateFlow(ORDER_KEY, Order())

    init {
        viewModelScope.launch {
            savedStateHandle[ORDER_KEY] = orderRepository.getOrder(orderId) ?: Order()
        }
    }

    fun onProductIdChange(productId: String): Boolean {
        var result = false

        if (productId.isNotBlank()) {
            val orderItems = order.value.orderItems.map{ orderItem ->
                if (orderItem.productId == productId) {
                    result = true
                    orderItem.copy(status = OrderItemStatus.PICKED)
                }
                else {
                    orderItem
                }
            }
            val allOrderItemsPicked = orderItems.all {
                orderItem -> orderItem.status == OrderItemStatus.PICKED
            }
            val orderStatus = if ( allOrderItemsPicked ) OrderStatus.READY else OrderStatus.IN_PROGRESS
            val readyAt = if (allOrderItemsPicked) Timestamp.now() else null
            savedStateHandle[ORDER_KEY] = order.value.copy(
                orderItems = orderItems.toMutableList(),
                status = orderStatus,
                readyAt = readyAt
            )
        }

        return result
    }

    fun save() {
        viewModelScope.launch {
            orderRepository.saveOrder(order.value)
        }
    }

    // Define a companion object
    companion object {
        // Define Log tag
        @Suppress("unused")
        private const val LOG_TAG = "ScanQrViewModel"

        // Define save state handle key's
        private const val ORDER_ID_KEY = "orderId"
        private const val ORDER_KEY = "order"

        // Define ViewModel factory
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val savedStateHandle = createSavedStateHandle()
                val mainApplication = this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as MainApplication
                OrderPickingViewModel(
                    savedStateHandle = savedStateHandle,
                    orderRepository = mainApplication.orderRepository
                )
            }
        }
    }
}