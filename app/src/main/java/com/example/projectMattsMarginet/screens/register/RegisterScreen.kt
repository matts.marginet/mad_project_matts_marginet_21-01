package com.example.projectMattsMarginet.screens.register

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.projectMattsMarginet.Screen
import com.example.projectMattsMarginet.components.ErrorMessage


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterScreen(
    navController: NavHostController,
    registerViewModel: RegisterViewModel = viewModel(factory = RegisterViewModel.Factory)
) {
    val userProfile by registerViewModel.userProfile.collectAsState()
    val password by registerViewModel.password.collectAsState()
    val passwordConfirmation by registerViewModel.passwordConfirmation.collectAsState()
    val errorMessage by registerViewModel.errorMessage.collectAsState()

    Column {
        ErrorMessage(errorMessage)
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Column {
                OutlinedTextField(
                    value = userProfile.email,
                    onValueChange = { newEmail ->
                        registerViewModel.onEmailChange(newEmail)
                    },
                    label = {
                        Text(text = "Email")
                    },
                    isError = errorMessage.isNotBlank()
                )
                OutlinedTextField(
                    value = userProfile.phoneNumber,
                    onValueChange = { newPhoneNumber ->
                        registerViewModel.onPhoneNumberChange(newPhoneNumber)
                    },
                    label = {
                        Text(text = "Phone Number")
                    },
                    isError = errorMessage.isNotBlank()
                )
                OutlinedTextField(
                    value = password,
                    onValueChange = { newPassword ->
                        registerViewModel.onPasswordChange(newPassword)
                    },
                    label = {
                        Text(text = "Password")
                    },
                    visualTransformation = PasswordVisualTransformation(),
                    isError = errorMessage.isNotBlank()
                )
                OutlinedTextField(
                    value = passwordConfirmation,
                    onValueChange = { newPasswordConfirmation ->
                        registerViewModel.onPasswordConfirmationChange(newPasswordConfirmation)
                    },
                    label = {
                        Text(text = "Password confirmation")
                    },
                    visualTransformation = PasswordVisualTransformation(),
                    isError = errorMessage.isNotBlank()
                )
                Button(
                    modifier = Modifier
                        .width(280.dp)
                        .padding(vertical = 15.dp),
                    onClick = {
                        registerViewModel.registerUser {
                            navController.navigate(Screen.Auth.Profile.route)
                        }
                    },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.secondary,
                        contentColor = MaterialTheme.colorScheme.onSecondary
                    )
                ) {
                    Text(text = "Register")
                }
            }
        }
    }
}