package com.example.projectMattsMarginet.models

import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Exclude
import kotlinx.parcelize.Parcelize

enum class OrderStatus(val displayName: String) {
    IN_PROGRESS("In Progress"),
    READY("Ready"),
    PICKED_UP("Picked up"),
    RETURNED("Returned")
}

@Parcelize
data class OrderStatusFilter(
    val status: OrderStatus,
    val checked: Boolean
): Parcelable

@Parcelize
@Suppress("unused")
data class Order(
    var orderId: String = "",
    var orderNumber: Int = 0,
    var uid: String = "",
    var readyAt: Timestamp? = null,
    var pickedUpAt: Timestamp? = null,
    var returnedAt: Timestamp? = null,
    val createdAt: Timestamp = Timestamp.now(),

    @get:Exclude
    var userProfile: UserProfile? = null,
    @get:Exclude
    val orderItems: MutableList<OrderItem> = mutableListOf(),
    @get:Exclude
    var status: OrderStatus = OrderStatus.IN_PROGRESS
): Parcelable {
    var statusOrdinal: Int
        get() = status.ordinal
        set(value) {status = OrderStatus.values().first { it.ordinal == value }}
}
