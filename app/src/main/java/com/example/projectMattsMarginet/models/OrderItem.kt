package com.example.projectMattsMarginet.models

import android.os.Parcelable
import com.google.firebase.firestore.Exclude
import kotlinx.parcelize.Parcelize

enum class OrderItemStatus(val displayName: String) {
    IN_PROGRESS("In progress"),
    PICKED("Picked")
}

@Parcelize
@Suppress("unused")
data class OrderItem(
    var orderItemId: String = "",
    val productId: String = "",

    @get:Exclude
    var product: Product? = null,
    @get:Exclude
    var status: OrderItemStatus = OrderItemStatus.IN_PROGRESS
): Parcelable {
    var statusOrdinal: Int
        get() = status.ordinal
        set(value) {status = OrderItemStatus.values().first { it.ordinal == value }}
}
