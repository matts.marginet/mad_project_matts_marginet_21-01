package com.example.projectMattsMarginet.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserProfile(
    var uid: String? = null,
    var token: String? = null,
    val email: String = "",
    val phoneNumber: String = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    val isAdmin: Boolean = false
): Parcelable
