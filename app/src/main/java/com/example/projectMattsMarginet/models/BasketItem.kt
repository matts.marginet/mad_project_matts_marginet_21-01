package com.example.projectMattsMarginet.models

import androidx.room.Entity
import androidx.room.PrimaryKey

// See https://developer.android.com/training/data-storage/room
@Entity
data class BasketItem(
    @PrimaryKey(autoGenerate = false)
    val productId: String
)
