package com.example.projectMattsMarginet.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    var productId: String = "",
    var pictureFileName: String = "",
    var inStock: Boolean = true,

    val title: String = "",
    val price: Float = 0F,
    val description: String = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    val isActive: Boolean = true
): Parcelable

