package com.example.projectMattsMarginet

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navigation
import com.example.projectMattsMarginet.repositories.AuthRepositoryImpl
import com.example.projectMattsMarginet.screens.productAddOrEdit.ProductAddOrEditScreen
import com.example.projectMattsMarginet.screens.productDetail.ProductDetailScreen
import com.example.projectMattsMarginet.screens.productList.ProductListScreen
import com.example.projectMattsMarginet.screens.login.LoginScreen
import com.example.projectMattsMarginet.screens.basket.BasketScreen
import com.example.projectMattsMarginet.screens.orderList.OrderListScreen
import com.example.projectMattsMarginet.screens.userProfile.ProfileScreen
import com.example.projectMattsMarginet.screens.register.RegisterScreen
import com.example.projectMattsMarginet.screens.orderPicking.OrderPickingScreen
import com.example.projectMattsMarginet.screens.statistics.StatisticsScreen
import com.example.projectMattsMarginet.screens.usage.UsageScreen
import com.example.projectMattsMarginet.screens.orderDatail.OrderDetailScreen
import com.example.projectMattsMarginet.ui.theme.ProjectMattsMarginetTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            ProjectMattsMarginetTheme{
                Scaffold(
                    modifier = Modifier
                        .navigationBarsPadding(),
                    bottomBar = {
                        BottomNavigationBar(items = listOf(
                            Screen.Home,
                            Screen.Product,
                            Screen.Basket,
                            Screen.Auth
                        ), navHostController = navController)
                    },
                    containerColor = MaterialTheme.colorScheme.background,
                    contentColor = MaterialTheme.colorScheme.onBackground
                ) {
                    innerPadding ->
                    Box(modifier = Modifier.padding(innerPadding)){
                        Navigation(
                            navController = navController
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun Navigation(
    navController: NavHostController
) {
    val mainApplication = LocalContext.current.applicationContext as MainApplication
    LaunchedEffect(key1 = true) {
        launch {
            // Needed if you close app while logged in, you need to set currentUserProfile on startup
            mainApplication.authRepository.getCurrentUserProfile()
        }
    }
    NavHost(navController = navController, startDestination = Screen.Home.route) {
        navigation(
            startDestination = Screen.Home.Usage.route,
            route = Screen.Home.route
        ){
            composable(Screen.Home.Usage.route) {
                val currentUserProfile = AuthRepositoryImpl.currentUserProfile
                if (currentUserProfile != null && currentUserProfile.isAdmin) {
                    OrderListScreen(navController = navController)
                } else {
                    UsageScreen()
                }
            }
            composable("${Screen.Home.OrderPicking.route}/{orderId}") {
                OrderPickingScreen(navController = navController)
            }
            composable(Screen.Home.Statistics.route) {
                StatisticsScreen()
            }
        }

        navigation(
            startDestination = Screen.Product.List.route,
            route = Screen.Product.route
        ){
            composable(Screen.Product.List.route) {
                ProductListScreen(navController = navController)
            }
            composable("${Screen.Product.AddOrEdit.route}/{productId}") {
                ProductAddOrEditScreen(navController = navController)
            }
            composable("${Screen.Product.Detail.route}/{productId}") {
                ProductDetailScreen(navController = navController)
            }
        }

        composable(Screen.Basket.route) {
            BasketScreen(navController = navController)
        }

        navigation(
            startDestination = Screen.Auth.Profile.route,
            route = Screen.Auth.route
        ){
            composable(Screen.Auth.Profile.route) {
                val currentUserProfile = AuthRepositoryImpl.currentUserProfile
                if (currentUserProfile != null){
                    ProfileScreen(navController = navController)
                } else {
                    LoginScreen(navController = navController)
                }
            }
            composable(Screen.Auth.Register.route) {
                RegisterScreen(navController = navController)
            }
            composable("${Screen.Auth.OrderDetail.route}/{orderId}") {
                OrderDetailScreen()
            }
        }
    }
}

@Composable
fun BottomNavigationBar(
    items : List<Screen>,
    navHostController: NavHostController,
    modifier: Modifier = Modifier
) {
    NavigationBar(
        modifier = modifier,
        containerColor = MaterialTheme.colorScheme.primary,
//        tonalElevation = 5.dp
    ) {
        val navBackStackEntry by navHostController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination

        items.forEach { item ->
            NavigationBarItem(
                selected = currentDestination?.hierarchy?.any{it.route == item.route} == true,
                onClick = {
                    navHostController.navigate(item.route){
                        popUpTo(navHostController.graph.findStartDestination().id){
                            inclusive = true
                            saveState = false
                        }
                        launchSingleTop = false
                        restoreState = false
                    }
                },
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = MaterialTheme.colorScheme.primary,
                    unselectedIconColor = MaterialTheme.colorScheme.onPrimary,
                    indicatorColor = MaterialTheme.colorScheme.onPrimary
                ),
                icon = { Icon(item.icon, contentDescription = null) }
            )
        }
    }
}

sealed class Screen(val route: String, val icon: ImageVector = Icons.Filled.Warning) {
    object Home: Screen("home", Icons.Filled.Home) {
        object Usage: Screen( "home_usage")
        object OrderPicking: Screen( "home_order_picking")
        object Statistics: Screen( "home_order_statistics")
    }
    object Product: Screen("product", Icons.Filled.List) {
        object List: Screen("product_list")
        object AddOrEdit: Screen("product_add_or_edit")
        object Detail: Screen("product_detail")
    }
    object Basket: Screen("basket", Icons.Filled.ShoppingCart)
    object Auth: Screen("auth", Icons.Filled.Person) {
        object Profile: Screen("auth_profile")
        object Register: Screen("auth_register")
        object OrderDetail: Screen("auth_order_detail")
    }
}
